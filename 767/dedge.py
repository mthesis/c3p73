import numpy as np
import math

from tensorflow.keras import backend as K
from tensorflow.keras.layers import Layer,Dense, Activation
import tensorflow.keras as keras# as k
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.optimizers import Adam,SGD
from tensorflow.linalg import trace





class dedge(Layer):
  def __init__(self,gs=20,param=40,k=8,batch_size=200,**kwargs):
    self.gs=gs
    self.param=param
    self.k=k
    self.batch_size=batch_size
    super(dedge,self).__init__(**kwargs)

  def build(self, input_shape):

    self.built=True
  def get_edge_feature(self,point_cloud, nn_idx, k=20):
    """Construct edge feature for each point
    Args:
      point_cloud: (batch_size, num_points, 1, num_dims)
      nn_idx: (batch_size, num_points, k)
      k: int
    Returns:
      edge features: (batch_size, num_points, k, num_dims)
    """


    point_cloud_central = point_cloud

    point_cloud_shape = point_cloud.shape
    batch_size = self.batch_size
    num_points = point_cloud_shape[1].value
    num_dims = point_cloud_shape[2].value


    #print("batch_size",batch_size,"num_points",num_points)
    #exit()

    idx_ = tf.range(batch_size) * num_points
    #print("hi")
    #exit()
    idx_ = tf.reshape(idx_, [batch_size, 1, 1]) 

    point_cloud_flat = tf.reshape(point_cloud, [-1, num_dims])
    point_cloud_neighbors = tf.gather(point_cloud_flat, nn_idx+idx_)
    #point_cloud_central = tf.expand_dims(point_cloud_central, axis=-2)

    point_cloud_central = tf.tile(point_cloud_central, [1, 1, k, 1])

    edge_feature = tf.concat([point_cloud_central, point_cloud_neighbors-point_cloud_central], axis=-1)
    return edge_feature

  def call(self,x):#inputs are an distance matrix (B,gs,gs) and a feature vector (B,gs,param), outputs a new feature vector
    a=x[0]
    f=x[1]
    
    _,nn_idx = tf.nn.top_k(-a, k=self.k)
    return self.get_edge_feature(f,nn_idx=nn_idx,k=self.k)
    
  def compute_output_shape(self,input_shape):
    a=input_shape[0]
    f=input_shape[1]

    print("dedgeinput",a,f)

    assert len(a)==3 and len(f)==3
    assert a[1]==self.gs
    assert a[2]==self.gs
    assert f[1]==self.gs
    assert f[2]==self.param
    return tuple([a[0],self.gs,self.k,2*self.param])#here no new parameters   

  def get_config(self):
    mi={"gs":self.gs,"param":self.param,"k":self.k,"batch_size":self.batch_size}
    th=super(dedge,self).get_config()
    th.update(mi)
    return th
  def from_config(config):
    return dedge(**config)































