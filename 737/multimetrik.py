import numpy as np
from numpy.random import randint as rndi
from numpy.random import random as rnd

from tensorflow.keras import backend as K
from tensorflow.keras.layers import Layer,Dense,Activation,Flatten,Dropout
import tensorflow.keras as keras# as k
import tensorflow as t
from tensorflow.keras.models import Sequential,load_model
from tensorflow.keras.optimizers import Adam,SGD,RMSprop
from tensorflow.keras.utils import plot_model
import json


import sys



gs=50
n=1000000



maxn=10
if len(sys.argv)>1:
  maxn=int(sys.argv[1])
alln=[]
for n in range(1,maxn+1):
  alln.append(n)

def makelenx(q,x):
  q=str(q)
  while len(q)<x:
    q="0"+q
  return q


print("generating",len(alln),"metriks")

from createmodel import objects as o
from advload import *

met=[]
nam=[]

for n in alln:

  for mi in ["b"]:

    model=aload("models/weights"+makelenx(n,4)+".h5",gs=gs,n=n)



    ll=model.layers

    submet=[]

    for l in ll:
      if l._name.find("gtopk")>-1:
        w=l.get_weights()
        acm=[]
        for e in w[0]:
          acm.append(float(e[0]))
        submet.append(acm)
    nam.append(str(n))
    met.append(submet)



 
with open("metriks.json","w") as f:
  f.write(json.dumps({"met":met,"nam":nam})) 
  
