#plots the parameter difference between the topdistribution and the qcd distribution as a function of the parameter indize
import numpy as np
import matplotlib.pyplot as plt

from adv2load import *

f=np.load("bcode.npz")

y=f["y"]
p=f["p"]

bins=20


offset=0

deltas=[]


for i in range(offset,len(p[0])):
  q=p[:,i]

  min=np.min(q)
  max=np.max(q)
  h0=np.histogram(q[y==0],bins=bins,range=(min,max))[0]
  h1=np.histogram(q[y==1],bins=bins,range=(min,max))[0]

   
 
  h0=h0/np.sum(h0)
  h1=h1/np.sum(h1)

  deltas.append(np.sum(np.abs(h0-h1)))

  print("did",i,len(p[0]))


plt.plot(np.arange(len(deltas)),deltas)

plt.xlabel("indize")
plt.ylabel("difference (seien hier a 0 und b 1, dann |a-b|, normiert auf 1)")



plt.savefig("differencebyindex.png",format="png")
plt.savefig("differencebyindex.pdf",format="pdf")

plt.show()


