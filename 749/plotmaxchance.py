import numpy as np
import matplotlib.pyplot as plt
import os

from nndraw import *

f=np.load("maxchance.npz")

c=f["c"]
p=f["p"]
ind=f["ind"]
l=f["l"]
inn=f["i"]




#best=ind[np.argmin(l)]
#print(best)
#exit()


outf="imgs/event"+str(inn)+"/"
os.makedirs(outf,exist_ok=True)


xlim=[-0.15,0.4]
ylim=[-0.25,0.15]

ot="plt.axis('off')"
ot=""

for i in range(len(ind)):
  #xlim=([np.min(p[:,:,1]),np.max(p[:,:1])+1])
  draw(ind[i],p[i],c[i],l[i],xlim=xlim,ylim=ylim,ot=ot)
  plt.savefig(outf+("%06d")%i+".png")
  #plt.show()
  print("did",i,"from",len(ind),"("+str(ind[i])+")")








