import tensorflow as tf
import tensorflow.keras as keras
import tensorflow.keras.backend as K
from tensorflow.keras.layers import Layer, Input, Dense, Conv2D, MaxPooling2D, Flatten
from tensorflow.keras.models import Model
from tensorflow.keras.losses import mse
from tensorflow.keras.optimizers import Adam,SGD,RMSprop
import sys
import numpy as np


class smult(Layer):
  def __init__(self,n=768,learnable=True,**kwargs):
    self.n=n
    super(smult,self).__init__(**kwargs)

  def build(self, input_shape):

    self.mult=self.add_weight(name="mult",
                                shape=(self.n,),
                                initializer="glorot_uniform",
                                trainable=True)



    self.built=True


  def call(self,x):
    x=x[0]
 
    #print("x",x.shape)
    #print("bias",self.bias.shape)
 
    ret=x*self.mult
    #print("ret",ret.shape) 

    return ret


    
  def compute_output_shape(self,input_shape):
    input_shape=input_shape[0]
    assert len(input_shape)==2
    assert input_shape[1]==self.n
    return tuple([input_shape[0],self.gs])    

  def get_config(self):
    mi={"n":self.n}
    th=super(smult,self).get_config()
    th.update(mi)
    return th
  def from_config(config):
    return smult(**config)





