import numpy as np
import matplotlib.pyplot as plt


f=np.load("evalb.npz")

p=f["p"]
c=f["c"]
y=f["y"]

my=np.mean(y)

for i in range(1,4):
  ap=p[:,:,i]
  ac=c[:,:,i]
  cora=[]
  corb=[]
  for j in range(len(p)):
    #print(ap[j],ac[j])
    if np.std(ap[j])<0.001 or np.std(ac[j])<0.001:
      #print(f"jumpin{j}")
      continue
    cc=np.corrcoef(ap[j],ac[j])[1,0]
    if y[j]<my:
      cora.append(cc)
    else:
      corb.append(cc)
    if not j%100:print(j)
  plt.close()
  print(f"{i} part a has mean correlation of {np.mean(cora)} +- {np.std(cora)}")
  print(f"{i} part b has mean correlation of {np.mean(corb)} +- {np.std(corb)}")
  plt.title(f"{i} (a) : {np.mean(cora):.4} +- {np.std(cora):.4}, (b): {np.mean(corb):.4} +- {np.std(corb):.4}")
  plt.hist(cora,bins=200,range=(-1,1),label=str(np.min(y)),color="blue",alpha=0.5)
  plt.hist(corb,bins=200,range=(-1,1),label=str(np.max(y)),color="orange",alpha=0.5)
  
  plt.legend()

  plt.savefig(f"imgs/cordif{i}.png",format="png")
  plt.savefig(f"imgs/cordif{i}.pdf",format="pdf")

  plt.show()


