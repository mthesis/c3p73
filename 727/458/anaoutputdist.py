import numpy as np
import matplotlib.pyplot as plt


f=np.load("evalb.npz")

p=f["p"]
c=f["c"]

def pre(q):
  return np.mean(np.abs(q),axis=(-1,-2))

p=pre(p)
c=pre(c)


plt.hist(p,alpha=0.5,label="prediction")
plt.hist(c,alpha=0.5,label="reality")

plt.legend()
plt.show()






































