#generates a lot of pictures, displaying the values of one parameter each, once for qcd and once for tops


import numpy as np
import matplotlib.pyplot as plt
import sys
#from adv2load import *

f=np.load("bcode.npz")

y=f["y"]
p=f["p"]

alpha=0.5
bins=20


offset=920
#offset=992
#offset=986
if len(sys.argv)>2:offset=int(sys.argv[2])


mmax=1000
mmax=960
if len(sys.argv)>3:mmax=int(sys.argv[3])

modus="and"
if len(sys.argv)>1:modus=sys.argv[1]

if modus=="and":
  base=np.zeros_like(p[:,0])!=1
else:
  base=np.zeros_like(p[:,0])==1
allsets=[]


for i in range(offset,mmax):
#q=np.sum(p,axis=-1)
  q=p[:,i]



  h,b=np.histogram(q,bins=bins)
  #b=[-10000000.0]+list(b)+[100000000.0]
  bb=[[b[i],b[i+1]] for i in range(len(b)-1)]
  bb=np.array(bb)





  #print(h)
  #print(np.sort(h))

  si=np.argsort(h)
  sh=h[si]
  sb=bb[si]



  #print(sh)
  #print(sb)

  noise=sb[-1]
  signal=sb[-2]
  sigrate=sh[-2]

  #if sigrate<1000:continue


  background=np.mean(sh[:-2])
  backgroundstd=np.std(sh[:-2])

  #print("background",background,"+-",backgroundstd)
  
  #print("sigrate",sigrate)

  delta=sigrate-background/(backgroundstd+0.001)
  #print(delta)
  #if delta>1000:continue

  #print("delta",delta)#,background,backgroundstd,sigrate)

  sigs=np.logical_and(q<=signal[1],q>=signal[0])

  allsets.append(sigs)

  sigtrue=np.sum(y[sigs])/sigrate
  #print(sigtrue)  
  #print(np.sum(y[sigs]))
  #print(signal)
  #print(noise)
  #print(sigrate) 
  #print(sh)


  #plt.close()

  #plt.hist(q[y==1],alpha=0.5,label="1")
  #plt.hist(q[y==0],alpha=0.5,label="0")
 
  #plt.legend()
  #plt.show()
  
  
  #if sigtrue<0.12:continue

  if sh[-3]>sh[-1]/10:
    print("jumped",i)
    #exit()
    continue
  
  

  if modus=="and":
    base=np.logical_and(base,sigs)
  else:
    base=np.logical_or(base,sigs)
  #print(np.sum(sigs),len(sigs),sigrate)


  #exit()

def eval(base,p=False):
  s=y[base]
  b=y[np.logical_not(base)]


  #true positive:test sagt ja, ist signal
  tp=np.sum(s)/len(s)
  #false positive#test sagt ja, ist aber kein signal
  fp=1-tp

  #false negative#test sagt nein, ist aber signal
  fn=np.sum(b)/len(b)
  #true negative#test sagt nein, ist auch kein signal
  tn=1-fn

  if p:
    print("tp",tp)
    print("fp",fp)
    print("fn",fn)
    print("tn",tn)
    print("size",np.sum(base))

  tpr=tp/(tp+fn)
  fpr=tn/(tn+fp)
  return tpr, fpr
def evalmask(bases,mask,p=False):
  base=np.zeros_like(bases[0])==1
  for i in range(len(mask)):
    if mask[i]:
      base=np.logical_or(base,bases[i])
  return eval(base,p=p)
def randommask(n,alpha=0.5):
  ret=np.zeros(n)==1
  for i in range(n):
    ret[i]=np.random.random()<alpha
  return ret

eval(base,p=True)
exit()




tpr,fpr=[],[]
k=1000
for i in range(k):
  at,af=evalmask(allsets,randommask(len(allsets)))
  tpr.append(at)
  tpr.append(af)
  fpr.append(af)
  fpr.append(at)
  print("did",i,k)

evalmask(allsets,randommask(len(allsets),alpha=12),p=True)


plt.plot(fpr,tpr,"o")
plt.show()












