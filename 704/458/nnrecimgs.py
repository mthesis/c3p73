import numpy as np
import matplotlib.pyplot as plt
import sys

from ndist import *
from nndraw import *

q="l>0"

histmode=False
cormode=False

if len(sys.argv)>1:
  if sys.argv[1]=="hist":histmode=True
  if sys.argv[1]=="cor":cormode=True
  if not (histmode or cormode):q=sys.argv[1]

jump=0
if len(sys.argv)>2:
  if sys.argv[2]=="hist":histmode=True
  if cormode:
    corA=sys.argv[2]
  else:
    if not histmode:jump=int(sys.argv[2])
jump0=jump

histtype="s"
if histmode and len(sys.argv)>3:
  histtype=sys.argv[3]
if cormode and len(sys.argv)>3:
  corB=sys.argv[3]

def test(q,i,l,s,x,y,c,p):#q,index,loss,"super" loss,x,y,to predict, predict
  return eval(q)


#def deltaloss(a,b):
#  c=np.concatenate((a,b))
#  d=np.max(c)-np.min(c)
#  return np.mean((a-b)**2)/d


#def new_loss(p,c):
#  pflag,peta,pphi,ppt=p[:,0],p[:,1],p[:,2],p[:,3]
#  cflag,ceta,cphi,cpt=c[:,0],c[:,1],c[:,2],c[:,3]

#  loss=0.0

#  eta=np.concatenate((peta,ceta))
#  phi=np.concatenate((pphi,cphi))

#  deta=np.max(eta)-np.min(eta)
#  dphi=np.max(phi)-np.min(phi)



#  #loss+=np.mean(((peta-ceta)/(deta+1))**2)
#  #loss+=np.mean(((pphi-cphi)/(dphi+1))**2)


#  loss+=deltaloss(pflag,cflag)
#  loss+=deltaloss(peta,ceta)
#  loss+=deltaloss(pphi,cphi)
#  loss+=deltaloss(ppt,cpt)



#  return loss



f=np.load("evalb.npz")
#f=np.load("bigevalb.npz")


p=f["p"]
c=f["c"]
y=f["y"]
x=f["x"]

l=np.mean((p-c)**2,axis=(-1,-2))
s=np.zeros_like(l)
for i in range(len(l)):
  s[i]=new_loss(p[i],c[i])

def findi(q,l,s,x,y,c,p,jump):
  for i in range(len(l)):
    if test(q,i,l[i],s[i],x[i],y[i],c[i],p[i]):
      jump-=1
      if jump<0:
        return i
  return -1

i=findi(q,l,s,x,y,c,p,jump)


if histmode:
  sp=[] 
  tohist=eval(histtype)
  print("histing shape",tohist.shape)
  for i in range(len(tohist)):
    if test(q,i,l[i],s[i],x[i],y[i],c[i],p[i]):
      sp.append(tohist[i])
  ran=(np.min(tohist),np.max(tohist))

  bins=30
  
  plt.hist(tohist,range=ran,bins=bins,log=True,color="black",alpha=0.2)

  plt.hist(sp,bins=bins,range=ran,log=True,alpha=0.8)

  plt.ylabel("if "+q)
  plt.xlabel(histtype)
  

  plt.title(str(len(sp))+" elements found")

  print(q," got ",len(sp))

  plt.show()
  exit()

if cormode:
  #print(corA,corB)
 

  cA=eval(corA)
  cB=eval(corB)

  plt.plot(cA,cB,"o",alpha=0.2)
  
  plt.xlabel(corA)
  plt.ylabel(corB)

  c=np.corrcoef(cA,cB)#[1,0]

  print(c)

  #plt.title("correlation of "+str(c))


  plt.show()
 


  exit()




draw(i,p[i],c[i],l[i])
plt.show()






















