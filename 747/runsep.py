import numpy as np
import sys
import csv
from agetsep import *

def makelenx(q,x):
  q=str(q)
  while len(q)<x:
    q="0"+q
  return q


def maxdex(j="b",**kwargs):#was so wrong....and i did not even need it here

  if j=="a":j="val_acc"
  if j=="b":j="val_loss"

  with open("history_sep.csv","r") as ff:
    c=csv.reader(ff,delimiter=",")
    jumped=False
    q=[]
    qn=[]
    epoch=[]
    for row in c:
      if not jumped:
        for e in row:
          q.append([])
          qn.append(e)
        jumped=True
        continue

      for i in range(len(row)):
        q[i].append(float(row[i]))


  row=[]

  for i in range(1,len(q)):
    if qn[i]==j:
      row=q[i]


  if j=="val_acc":
    ii=np.argmax(row)
  else:
    ii=np.argmin(row)

  return ii




valsplit=0.1
keepsplit=0.3

f=np.load("bcode.npz")

x=f["p"]
trenn=int(valsplit*len(x))
trenn2=int((1-keepsplit)*len(x))

x=x[trenn2:]
y=f["y"][trenn2:]

print(len(x),len(y))


model=getsep()
model.load_weights("modelsepb.tf")
model.summary()


s=model.predict(x)

np.savez_compressed("sep",s=s,x=x,y=y)


print("done")

