#plots the parameter difference between the topdistribution and the qcd distribution as a function of the parameter indize
#generalisation of anavenc to more datarows (especcially 2)


import numpy as np
import matplotlib.pyplot as plt

from adv2load import *

f=np.load("bcode.npz")
f2=np.load("bcodesv.npz")

y=np.concatenate((f["y"],f2["y"]))
p=np.concatenate((f["p"],f2["p"]))

bins=20


offset=0

ymax=int(np.max(y))
for i1 in range(ymax+1):
  for i2 in range(i1+1,ymax+1):

    plt.close()
    deltas=[]
    for i in range(offset,len(p[0])):
      q=p[:,i]

      min=np.min(q)
      max=np.max(q)
      hs=[]
      for k in range(ymax+1):
        hs.append(np.histogram(q[y==k],bins=bins,range=(min,max))[0])
        hs[-1]=hs[-1]/np.sum(hs[-1])
   
 

      deltas.append(np.sum(np.abs(hs[i1]-hs[i2])))

      print("did",i,len(p[0]))

    plt.title("difference by index of "+str(i1)+" and "+str(i2))
    plt.plot(np.arange(len(deltas)),deltas)

    plt.xlabel("indize")
    plt.ylabel("difference (seien hier a "+str(i1)+" und b "+str(i2)+", dann |a-b|, normiert auf 1)")



    plt.savefig("differencebyindex"+str(i1)+"-"+str(i2)+".png",format="png")
    plt.savefig("differencebyindex"+str(i1)+"-"+str(i2)+".pdf",format="pdf")

    plt.show()


