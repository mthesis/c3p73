import numpy as np
import matplotlib.pyplot as plt


invert=True




f=np.load("evalb.npz")

x=f["x"]
y=f["y"]
p=f["p"]
c=f["c"]

fp=np.load("bigevalb.npz")["c"]

p=np.mean(c,axis=0)

#print(c.shape,p.shape)
#exit()

#print(y)

#exit()

def difference(a,b):
  #print(a.shape,b.shape,np.mean(a-b).shape)
  #exit()
  return np.sqrt(np.mean((a-b)**2))

d=np.zeros((len(y),))

d0,d1=[],[]

for i in range(len(y)):
  d[i]=difference(c[i],p)
  if (y[i]>0.5) != invert:
#  if np.random.randint(2)==0:
    d1.append(d[i])
  else:
    d0.append(d[i])

#print(d)
#print(d.shape)

#exit()

cmax=np.max(d)
cmin=np.min(d)
cdel=(cmax-cmin)/1000
if cdel==0:
  print("just 0 difference")
  exit()
cs=np.arange(cmin,cmax,cdel)
tpr,fpr,tnr,fnr=[],[],[],[]

for c in cs:
  tp,fp,tn,fn=0,0,0,0
  for ad in d0:
    if ad>c:
      fp+=1#false positive, is zero, classified as one
    else:
      tn+=1#true negative, is zero, classified as zero
  for ad in d1:
    if ad>c:
      tp+=1#true positive, is one, classified as one
    else:
      fn+=1#false negative, is one, classified as zero
  tpr.append(tp/(tp+fn))
  fpr.append(fp/(fp+tn))
  tnr.append(tn/(tn+fp))
  fnr.append(fn/(fn+tp))

def int(x,y):
  x,y=(list(t) for t in zip(*sorted(zip(x,y))))
  ret=0.0
  for i in range(1,len(x)):
    ret+=((y[i]+y[i-1])*(x[i]-x[i-1]))/2
  return ret


np.savez_compressed("meanroc",c=c,tpr=tpr,fpr=fpr,tnr=tnr,fnr=fnr)

#plt.hist(d,bins=200)
plt.hist(d0,bins=100,alpha=0.5,label="0")
plt.hist(d1,bins=100,alpha=0.5,label="1")

plt.legend()

plt.xlabel("mse")
plt.ylabel("#")

plt.savefig("imgs/meanrecqual.png",format="png")
plt.savefig("imgs/meanrecqual.pdf",format="pdf")

plt.show()

plt.close()

auc=int(fpr,tpr)

plt.plot(fpr,tpr,label="auc="+str(round(auc,4)),color="darkblue")
plt.plot(tpr,fpr,label="auc="+str(round(1-auc,4)),color="dodgerblue")

plt.plot(np.arange(0,1,0.01),np.arange(0,1,0.01),color="grey",alpha=0.3)



plt.legend()
plt.xlabel("false positive rate (is true, but classified as false)")
plt.ylabel("true positive rate (is true, and classified as true)")

plt.savefig("imgs/meanroc.png",format="png")
plt.savefig("imgs/meanroc.pdf",format="pdf")
plt.show()


















