import sys
import numpy as np
import matplotlib.pyplot as plt
import os
import math


from trainingon import trainingon

from op1 import optimize as opt

from cauc import caucd


base=[1,3,9,27,9,3,1]
base=[1,0.00001]

def trainonwhich(trainingon):
  if "1" in trainingon:return 1
  if "0" in trainingon:return 0
  return -1



def statinf(q):
  return {"shape":q.shape,"mean":np.mean(q),"std":np.std(q),"min":np.min(q),"max":np.max(q)}

def getc():
  return np.sum([1 for q in os.listdir(".") if "seval" in q and ".npz" in q])

def loaddata(n=None):
  if n is None:n=getc()
  ret=[]
  for i in range(1,n+1):
    f=np.load("seval"+str(i)+".npz")
    ret.append(f["s"])
  ret=np.array(ret)
  ret=np.mean(ret,axis=-1)
  ret=np.transpose(ret)
  return ret,f["y"]




if __name__=="__main__":
  n=None
  try:
    if len(sys.argv)>1:n=int(sys.argv[1])
  except:pass

  if n is None:n=getc()
  t,y=loaddata(n)

  t0=t

  if trainonwhich(trainingon)>0.5:
    t=t[np.where(y>0.5)]
  else:
    t=t[np.where(y<0.5)]

  np.savez_compressed("toc.npz",t=t,t0=t0,y=y,n=n)



  print(t.shape,t0.shape)
  
  o,yt=opt(np.transpose(t))

  print(statinf(o))

  ot=np.dot(t0,np.reshape(yt,(len(yt),1)))[:,0]
  
  print(statinf(ot))



  o0=np.mean([np.abs(q0-np.mean(q)) for q0,q in zip(np.transpose(t0),np.transpose(t))],axis=0)
  o1=np.mean([np.abs(q-np.mean(q)) for q in np.transpose(t)],axis=0)


  print("ss",np.std(o1))
  print("ms",np.std(o)/np.sum(yt))
  print(np.std(t,axis=-1)[:5]) 


  q=caucd(d=np.abs(ot-1),y=y)
  qs=caucd(d=o0,y=y)

  print("auc",q["auc"],qs["auc"])
  #print(q["e30"],qs["e30"])



  exit()
























