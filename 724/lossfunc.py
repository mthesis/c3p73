

def adaptmean(q,O):
  if len(q.shape)>=2:
    return O.mean(q,axis=(-1,-2))
  if len(q.shape)==1:
    return O.mean(q)
  return q

def powerloss(a,b,O,pwr=2):
  d=O.abs(a-b)**pwr
  return adaptmean(d,O)**(1/pwr)
  return O.mean(d,axis=(-1,-2))**(1/pwr)

def rootplusloss(a,b,O,alpha=1.0):
  d=O.sqrt(alpha+O.abs(a-b))
  return (adaptmean(d,O)-O.sqrt(alpha))**2
  return (O.mean(d,axis=(-1,-2))-O.sqrt(alpha))**2

def currentloss(a,b,O,finalise=True):
  """calculcates the difference between a and b using O, assumes shape to be 3d, does not need the first dimension to be specified, obviously a and b need to have the same shape"""

  #print(a.shape,b.shape)
  #exit()

  
  q=powerloss(a,b,O,pwr=2)

  if finalise:
    return O.mean(q)
  else:
    return q



