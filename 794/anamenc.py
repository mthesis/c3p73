#same as anavenc, but modular 40 (parametersize)
import numpy as np
import matplotlib.pyplot as plt

from adv2load import *

f=np.load("bcode.npz")

y=f["y"]
p=f["p"]

bins=20


offset=0
k=40



deltas=[]
for i in range(k):
  deltas.append([])


for i in range(offset,len(p[0])):
  q=p[:,i]

  min=np.min(q)
  max=np.max(q)
  h0=np.histogram(q[y==0],bins=bins,range=(min,max))[0]
  h1=np.histogram(q[y==1],bins=bins,range=(min,max))[0]

   
 
  h0=h0/np.sum(h0)
  h1=h1/np.sum(h1)

  deltas[i%k].append(np.sum(np.abs(h0-h1)))

  print("did",i,len(p[0]))


m=np.mean(deltas,axis=-1)
s=np.std(deltas,axis=-1)


plt.errorbar(np.arange(k),m, yerr=s)

plt.xlabel("indize")
plt.ylabel("difference (seien hier a 0 und b 1, dann |a-b|, normiert auf 1)")



plt.savefig("differencebyindexmod"+str(k)+".png",format="png")
plt.savefig("differencebyindexmod"+str(k)+".pdf",format="pdf")

plt.show()


