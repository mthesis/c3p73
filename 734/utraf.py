import numpy as np
from os import listdir
import matplotlib.pyplot as plt


p_id=0#particle id
f_id=0#feature id




n=len(listdir("data/ueval"))



def norm(q):
  return (q-np.mean(q,axis=0))/np.std(q,axis=0)
def comp(a,b):
  return np.sqrt(np.sum((a-b)**2,axis=0))/a.shape[0]
def mcomp(q):
  ret=[]
  for i in range(1,len(q)):
    ret.append(comp(q[i-1],q[i]))
  return np.array(ret)

ds=[]
for i in range(1,n+1):
  ac=np.load("data/ueval/eval"+str(i)+".npz")
  p=ac["p"]#[:,p_id,f_id]
  c=ac["c"]#[:,p_id,f_id]
  d=norm((p-c))
  ds.append(d)
  print("did",i)

if len(ds)==0:exit()

c=mcomp(ds)



np.savez_compressed("data/uc",c=c)

#c=c[:,p_id,f_id]

for i in range(c.shape[1]):
  for j in range(c.shape[2]):
    ac=c[:,i,j]
    plt.plot(ac,label=str(i)+"*"+str(j))
    print(str(i)+"*"+str(j),np.mean(ac))

#plt.plot(c)
#plt.title(str(p_id)+"*"+str(f_id))

plt.ylabel("delta")
plt.xlabel("epoch")

plt.savefig("imgs/utraf.png",format="png")
plt.savefig("imgs/utraf.pdf",format="pdf")



plt.show()







