import os
from os import path as op
import sys

max=100
if len(sys.argv)>1:
  max=int(sys.argv[1])

folds=[int(q) for q in os.listdir("multi/") if op.isfile(f"multi/{q}/evalb.npz")]
folds=folds[:max]



for fold in folds:
  os.system(f"python3 predictencode.py {fold}")


for fold in folds:
  os.system(f"python3 predictbigencode.py {fold}")



for i in range(100):

  for fold in folds:
    os.system(f'python3 sep.py {fold} "[{i},{i}]"')
    os.system(f"python3 sepqual.py {fold}")


