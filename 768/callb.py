

from tensorflow.keras.callbacks import Callback

import numpy as np



class BatchStore(Callback):
  def __init__(self, N,nam="amodels/weigths"):
    self.N = N
    self.nam=nam

  def on_epoch_begin(self, epoch, logs={}):
    self.epoch = epoch
  def on_batch_begin(self, batch, logs={}):
    if batch % self.N == 0:
        name = (self.nam+'%04d_%04d.tf') % (self.epoch,batch)
        self.model.save_weights(name)
class LossHistory(Callback):#simply save every loss
    def on_train_begin(self, logs={}):
        self.losses = []
        self.acepoch=0
        self.epochs=[]
        self.batches=[]
    def on_batch_end(self, batch, logs={}):
        self.losses.append(logs.get('loss'))
        self.epochs.append(self.acepoch)
        self.batches.append(batch)

    def on_epoch_begin(self,epoch,logs=None):
        self.acepoch=epoch
    def on_epoch_end(self,epoch,logs=None):
        np.savez_compressed("batchloss",l=self.losses,e=self.epochs,b=self.batches)


