import numpy as np
import matplotlib.pyplot as plt

from loaddata import *

from cauc import caucd


x=loadevalplus()
_,y=loadeval()


y0=np.where(y<0.5)
y1=np.where(y>0.5)

n=int(x.shape[-1])

for i in range(n):
  plt.close()

  a=x[y0,i][0]
  b=x[y1,i][0]

  print(a.shape,b.shape)
  

  plt.hist(a,alpha=0.5,label="qcd",bins=100)
  plt.hist(b,alpha=0.5,label="top",bins=100)

  print("histed")
 
  auc=caucd(d=x[:,i],y=y)["auc"]
  print("auc")
  plt.title(str(auc))
  print(auc)

  plt.legend()

  plt.show()
  


