import numpy as np
import os
from os.path import isfile

def statinf(q):
  return {"shape":np.array(q).shape,"mean":np.mean(q),"std":np.std(q),"max":np.max(q),"min":np.min(q)}


fns=["data/"+q for q in os.listdir("data") if isfile("data/"+q)]

fs=[np.load(fn) for fn in fns]

pds=np.array([f["pd"] for f in fs])
wds=np.array([f["wd"] for f in fs])


pds=np.mean(pds,axis=0)
wds=np.mean(wds,axis=0)

w5=(-wds).argsort()[-5:][::-1]
p5=(pds).argsort()[-5:][::-1]






from tensorflow.keras.datasets import mnist
print(statinf(wds))
print(statinf(wds))

(x_train, y_train), (x_test, y_test) = mnist.load_data()


def getdata(norm=True,normdex=7,n=1000):
  if norm:
    ids=np.where(y_train==normdex)
  else:
    ids=np.where(y_train!=normdex)
  qx=x_train[ids][:n]
  return qx

normdex=7
t=getdata(norm=True,normdex=normdex,n=5000)
at=getdata(norm=False,normdex=normdex,n=5000)

pp=t[p5]
pw=at[w5]


num=10
images=np.concatenate((pp,pw),axis=0)
labels=[str(np.round(q,5)) for q in np.concatenate((pds[p5],wds[w5]),axis=0)]

import matplotlib.pyplot as plt


num_row = 2
num_col = 5
# plot images
fig, axes = plt.subplots(num_row, num_col, figsize=(1.5*num_col,2*num_row))
for i in range(num):
    ax = axes[i//num_col, i%num_col]
    ax.imshow(images[i], cmap='gray')
    ax.set_title('Loss: {}'.format(labels[i]))
plt.tight_layout()

plt.savefig("examples.png",format="png")
plt.savefig("examples.pdf",format="pdf")


plt.show()







