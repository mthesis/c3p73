import numpy as np
from numpy.random import randint as rndi
from numpy.random import random as rnd

from tensorflow.keras import backend as K
from tensorflow.keras.layers import Layer,Dense,Activation,Flatten,Dropout
import tensorflow.keras as keras# as k
import tensorflow as t
from tensorflow.keras.models import Sequential,load_model
from tensorflow.keras.optimizers import Adam,SGD,RMSprop
from tensorflow.keras.utils import plot_model
import json


from createmodel import objects as o


for mi in ["b","a"]:

  model=load_model("model"+mi+".h5",custom_objects=o)

  print("loaded ",mi)
  model.summary()
  print("input",model.input_shape)
  #print("output",model.compute_output_shape(model.input_shape))

  data=np.zeros((12,30,4))
  
  pred=model.predict(data)

  print("pred",pred.shape)
  print(pred[0])

  exit()
