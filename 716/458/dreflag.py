import numpy as np
import math

from tensorflow.keras import backend as K
from tensorflow.keras.layers import Layer,Dense, Activation
import tensorflow.keras as keras# as k
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.optimizers import Adam,SGD
from tensorflow.linalg import trace





class dreflag(Layer):
  def __init__(self,gs=20,param=40,flagI=7,flagO=0,**kwargs):
    self.gs=gs
    self.param=param
    self.flagI=flagI
    self.flagO=flagO
    super(dreflag,self).__init__(**kwargs)

  def build(self, input_shape):

    self.built=True

  def call(self,x):
    a=x[0]
  
    return tf.roll(a,shift=-((self.flagI-self.flagO) % self.param),axis=-1)

 
    rowO=a[:,:,self.flagO]
    rowI=a[:,:,self.flagI]

    a[:,:,self.flagI]=rowO
    a[:,:,self.flagO]=rowI


    return a

 
  def compute_output_shape(self,input_shape):
    a=input_shape[0]
    assert len(a)==3
    assert a[1]==self.gs
    assert a[2]==self.param
    return tuple([input_shape[0],self.gs,self.param])   

  def get_config(self):
    mi={"gs":self.gs,"param":self.param,"flagO":self.flagO,"flagI":self.flagI}
    th=super(dreflag,self).get_config()
    th.update(mi)
    return th
  def from_config(config):
    return dreflag(**config)































