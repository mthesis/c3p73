import tensorflow as tf
import tensorflow.keras as keras
import tensorflow.keras.backend as K
from tensorflow.keras.layers import Layer, Input, Dense, Conv2D, MaxPooling2D, Flatten
from tensorflow.keras.models import Model
from tensorflow.keras.losses import mse
from tensorflow.keras.optimizers import Adam,SGD,RMSprop
import sys
import numpy as np
import matplotlib.pyplot as plt
import os

from smult import *



base=[1,3,9,27,9,3,1]

def statinf(q):
  return {"shape":q.shape,"mean":np.mean(q),"std":np.std(q),"min":np.min(q),"max":np.max(q)}


def getmodel(q,reg=None,act="relu",mean=1.0,seed=None):
  if not seed is None:np.random.seed(seed)
  if not seed is None:tf.random.set_seed(seed)
  #inn=Input(shape=(28,28,1))
  inn=Input(shape=(q[0],))
  w=inn
  for aq in q[1:]:
    w=Dense(aq,activation=act,use_bias=False,kernel_initializer=keras.initializers.TruncatedNormal(),kernel_regularizer=reg)(w)
  w=smult(n=q[-1])([w])
  m=Model(inn,w,name="oneoff")
  zero=K.ones_like(w)*mean
  loss=mse(w,zero)
  loss=K.mean(loss)
  m.add_loss(loss)
  m.compile(Adam(lr=0.01))
  return m



def trainone(t,index,n,l):
  cb=[keras.callbacks.EarlyStopping(monitor='val_loss',patience=20,restore_best_weights=True),keras.callbacks.TerminateOnNaN()]
  if index is None:
    cb.append(keras.callbacks.ModelCheckpoint(f"msMep{n}.tf", monitor='val_loss', verbose=1, save_best_only=True,save_weights_only=True))
    cb.append(keras.callbacks.CSVLogger(f"history_sMep{n}.csv"))
  else:
    cb.append(keras.callbacks.ModelCheckpoint(f"multi/{index}/msMep{n}.tf", monitor='val_loss', verbose=1, save_best_only=True,save_weights_only=True))
    cb.append(keras.callbacks.CSVLogger(f"multi/{index}/history_sMep{n}.csv"))

  m=getmodel(l)
  m.summary()
  h=m.fit(t,None,
          epochs=500,
          batch_size=100,
          validation_split=0.25,
          verbose=1,
          callbacks=cb)



def increase(q,mx):
  for i in range(len(q)):
    q[i]+=1
    if q[i]<mx-1:break
    if i==len(q)-1:return q,True
    q[i]=q[i+1]+1
      
  return q,False

def valid(q):
  for i in range(len(q)-1):
    if q[i]<=q[i+1]:return False
  return True

def koutofn(k,n):
  ret=[]
  base=list([i for i in range(n)])
  
  its=list([k-i-1 for i in range(k)])
  
  while True:
    if valid(its):
      ac=list([base[it] for it in its])
      ret.append(ac)
    its,stp=increase(its,mx=n+1)
    # print("!","increased to",its,stp)
    if stp:break
    
  return ret

def allc(c,minc=4,deltac=2):
  ret=[]
  for i in range(minc,c+1-deltac):
    for q in koutofn(i,c):
      ret.append(q)
  np.random.seed(12)
  np.random.shuffle(ret)
  return ret



if __name__=="__main__":
  index=None
  try:
    if len(sys.argv)>1:index=int(sys.argv[1])
  except:pass

  if index is None:
    f=np.load("bigcode.npz")
  else:
    f=np.load(f"multi/{index}/bigcode.npz")

  multiplicator=1


  t=f["p"]
  #print(t.shape)
  mult=int(t.shape[-1])

  jobs=allc(mult)

  print(f"working on {len(jobs)} jobs")

  for idd,job in enumerate(jobs):

    for d in range(10):
      print(f"working on job {idd}=>{job}")

    l=list([len(job)*ac for ac in base])


    for i in range(0,multiplicator):
      trainone(t=t[:,job],index=index,n=idd,l=l)
      os.system("python3 sMeval.py zero "+str(i))




















