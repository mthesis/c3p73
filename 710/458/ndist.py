import numpy as np




def deltaloss(a,b):
  c=np.concatenate((a,b))
  d=np.max(c)-np.min(c)
  return np.mean((a-b)**2)/d

def new_loss(p,c):
  pflag,peta,pphi,ppt=p[:,0],p[:,1],p[:,2],p[:,3]
  cflag,ceta,cphi,cpt=c[:,0],c[:,1],c[:,2],c[:,3]

  loss=0.0

  eta=np.concatenate((peta,ceta))
  phi=np.concatenate((pphi,cphi))

  deta=np.max(eta)-np.min(eta)
  dphi=np.max(phi)-np.min(phi)



  #loss+=np.mean(((peta-ceta)/(deta+1))**2)
  #loss+=np.mean(((pphi-cphi)/(dphi+1))**2)


  loss+=deltaloss(pflag,cflag)
  loss+=deltaloss(peta,ceta)
  loss+=deltaloss(pphi,cphi)
  loss+=deltaloss(ppt,cpt)



  return loss

