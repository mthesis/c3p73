#generates a lot of pictures, displaying the values of one parameter each, once for qcd and once for tops


import numpy as np
import matplotlib.pyplot as plt

from adv2load import *

f=np.load("bcode.npz")

y=f["y"]
p=f["p"]
m1=f["m1"]
m2=f["m2"]
m3=f["m3"]
m4=f["m4"]
m5=f["m5"]
m6=f["m6"]

alpha=0.5
bins=20


offset=0

for i in range(offset,len(p[0])):
#q=np.sum(p,axis=-1)
  q=p[:,i]

  plt.title(str(np.mean(q))+","+str(np.std(q))+","+str(np.min(q))+","+str(np.max(q)))


  plt.hist(q[y==0],alpha=alpha,bins=bins,label="0")
  plt.hist(q[y==1],alpha=alpha,bins=bins,label="1")
  plt.legend()
  plt.savefig("imgs/multiana"+makelenx(i,3)+".png",format="png")
  print("did",i,len(p[0]))
  plt.close()

