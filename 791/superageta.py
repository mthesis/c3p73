from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from tensorflow.keras.layers import Lambda, Input, Dense
from tensorflow.keras.models import Model
from tensorflow.keras.datasets import mnist
from tensorflow.keras.losses import mse, binary_crossentropy
from tensorflow.keras.utils import plot_model
from tensorflow.keras import backend as K

import tensorflow as tf

import numpy as np
import matplotlib.pyplot as plt
import argparse
import os

from createmodel import *
from supercreatemodel import *
from ageta import *





def getsupervised():
  model=createsupervised(gs=gs)

  model.summary()
  plot_model(model, to_file='supervised.png', show_shapes=True)

  model.compile(Adam(lr=learning_rate),loss="categorical_crossentropy",metrics=["accuracy"])
  return model

